package dataCreator;

/**
 * creates an articial WebLog 
 * @author Tareq
 */
import java.io.FileWriter;
import java.util.GregorianCalendar;

public class ArtificialWebLog {
	
	
	public static void generateWebLog(String fileName, long size) throws WebLogGeneratorException
	{
		String writer = null;
        GregorianCalendar cal = new GregorianCalendar();
		try(FileWriter write = new FileWriter(fileName))
		{
			for (int i = 0; i < size; i++) {
				
				writer = ((int)(Math.random()*256) + "." + (int)(Math.random()*256)+ "." + (int)(Math.random()*256)+ "." +  (int)(Math.random()*256) + "$ - - ");
				
				writer += "[" +  cal.getTime()+":"+i+ "]" ;
				
				writer += " \"" + "GET /" + "FILENR" + i +  "\"" + "\n";
				
				write.write(writer);
				
			}
			
		} catch (Exception e) {
			throw new WebLogGeneratorException("LOGGING ERROR");
		}
		
	}

}
