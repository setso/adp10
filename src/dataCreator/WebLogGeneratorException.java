package dataCreator;

/**
 * A WebLogGeneratorException indicates that the logging of a weblog operation has failed 
 * @author Tareq
 */
public class WebLogGeneratorException extends Exception
{

    private static final long serialVersionUID = 1L;

    /**
     * Initialisiert eine neue Exception mit der übergebenen Fehlermeldung.
     * 
     * @param message Eine Fehlerbeschreibung.
     */
    public WebLogGeneratorException(String message)
    {
        super(message);
    }
}
