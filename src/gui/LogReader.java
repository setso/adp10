package gui;

import java.io.BufferedReader;
import java.io.FileReader;

import hashing.HashTable;
import hashing.Row;

public class LogReader {
	
	HashTable ht;
	
	public LogReader(HashTable hshtbl, String path) {
		this.ht = hshtbl;
		parse(path);
	}

	private void parse(String path) {
		String row;
		try(BufferedReader reader = new BufferedReader(new FileReader(path));) 
		{
			while((row = reader.readLine()) != null)
			{
				ht.insert(new Row(row));
			}
		} catch (Exception e) {
            e.printStackTrace();
            System.out.println("Datei nicht gefunden");
            }
		
	}

}
