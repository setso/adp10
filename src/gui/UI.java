package gui;

import hashing.HashTable;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class UI extends Application{
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	private static final String LOGPATH = "log.txt";

	@Override
	public void start(Stage stage) throws Exception {
		HashTable hashTable = new HashTable();
		LogReader logreader = new LogReader(hashTable, LOGPATH);
		 hashTable.displayTable();
		
		stage.setTitle("UI - HashTable - DH");
		Group root = new Group();
		Scene scene = new Scene(root,700,530,Color.AQUA);
		
		GridPane gpane = new GridPane();
		gpane.setPadding(new Insets(5));
		gpane.setHgap(10);
		gpane.setVgap(10);
		
		Label lb1 = new Label("IP - Adresse");
		GridPane.setHalignment(lb1, HPos.LEFT);
		gpane.add(lb1,0,0);
		
        Label lb2 = new Label("Data:");
        gpane.add(lb2, 2, 0);
        GridPane.setHalignment(lb2, HPos.LEFT);
        
        final ObservableList<String> ipAdressList = FXCollections.observableArrayList();
        ipAdressList.addAll(hashTable.getAllIPs());
        final ListView<String> leftListView = new ListView<String>(ipAdressList);
        leftListView.setPrefWidth(150);
        leftListView.setPrefHeight(150);
        lb1.setText(String.format("IP-Adress:  %d",ipAdressList.size()));
        
        gpane.add(leftListView, 0, 1);
        
        final ObservableList<String> dataList = FXCollections.observableArrayList();
        final ListView<String> dataListView = new ListView<String>(dataList);
        dataListView.setPrefWidth(470);
        dataListView.setPrefHeight(490);
        
        gpane.add(dataListView, 2, 1);
        
        Button showDataButton = new Button("Show");
        showDataButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String item = leftListView.getSelectionModel().getSelectedItem();
                if (item != null) {
                    dataList.clear(); 
                    Long key = Long.parseLong(item.replace(".",""));
                    dataList.addAll(hashTable.getAllLogMessagesForKey(key));
                    System.out.println(hashTable.getAndResetCounter());
                   
                    leftListView.getSelectionModel().clearSelection();
                }
            }
        });
		
		
		
		VBox vbox = new VBox(5);
		vbox.getChildren().addAll(showDataButton);
		gpane.add(vbox, 1, 1);
		
		root.getChildren().add(gpane);
		stage.setScene(scene);
		stage.show();
		
	}
	
	
}
