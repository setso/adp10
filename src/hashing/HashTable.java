package hashing;

import java.util.HashSet;
import java.util.Set;



public class HashTable {
	
	private Entry[] entrys;
	private int load;
	private int maxLoad;
    private int counter;
	
	public HashTable() {
		maxLoad = 15;
		entrys = new Entry[maxLoad];
		load = 0;
		counter = 0;
	}
	
	
//	private int HashFunction1(long key)
//	{
//		return (int)key % maxLoad;
//	}
//	
//	private int HashFunction2(long key)
//	{
//		return (int)(1+(key % (maxLoad-2)));
//	}
	
	public void insert(Row row)
	{
		int returnVal = -1;
        int offset = 1;
        boolean finished = false;
        
        while(!finished){
            returnVal = getHash(row.getKey(), offset);
            if( entrys[returnVal] != null && !entrys[returnVal].getDeletedFlag()){ 
                offset++;
                if(checkLoadFactor()){
                    resize();
                } 
            } else {
                entrys[returnVal] = new Entry(row);
                load++;
                finished = true;
            }
        }
    }
	
    private void resize() {
        maxLoad = load*2;
        Entry[] oldArr = entrys;
        entrys= new Entry[maxLoad];
        
        for(int i = 0; i < oldArr.length; i++){
            if(oldArr[i] != null){
                insert(oldArr[i].getData());
            }
        }  
    }
	
    private boolean checkLoadFactor() {
        return (0.8 * maxLoad ) < load ;
    }
	
	private int getHash(long key, int offset){
        int index = (int)(( ((long)key % maxLoad) + ((1 + ((long)key % (maxLoad -2))) * (int) (Math.pow(offset, 2))) )  % maxLoad);
        return index;
    }
	
    public void displayTable() {
	    System.out.print("Table: ");
	    for (int j = 0; j < maxLoad; j++) {
	      if (entrys[j] != null)
	        System.out.print("Index:" + j+"Log: " +  entrys[j].getData().getIP());
	      else
	        System.out.print("** ");
	    }
	    System.out.println(entrys.length);
	  }
    
    public Set<String> getAllIPs(){
        Set<String> set = new HashSet<String>();
        for(Entry elem: entrys){
            if(elem != null && elem.getData() != null){
                set.add(elem.getData().getIP());
            }
        }
        return set;
    }
    
    public Set<String> getAllLogMessagesForKey(long l){
        Set<String> set = new HashSet<String>();
        int returnVal = -1;
        int offset = 1;
        boolean finished = false;
        
        while(!finished){
            counter++;
            returnVal = getHash(l, offset);
            if( entrys[returnVal] != null && entrys[returnVal].getData().getKey() == l){
                set.add(entrys[returnVal].getData().getRow());
                offset++;
            } else if(entrys[returnVal] != null){
                offset++;
            } else {
                finished = true;
            }
        }
        
        return set;
    }
    
    public int getAndResetCounter() {
        int ret = counter;
        counter = 0;
        return ret;
    } 

}
