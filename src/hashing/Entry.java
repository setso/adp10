package hashing;

public class Entry {

	private boolean deletedFlag;
	private Row data;
	
	public Entry() {
		deletedFlag = false;
	}
	public Entry(Row row)
	{
		this.data = row;
		deletedFlag = false;
	}
	
	public boolean getDeletedFlag()
	{
		return deletedFlag;
	}
	
	public Row getData()
	{
		return data;
	}
}
