package hashing;

public class Row {
	
	private String data;
	private String ip;
	private long key;
	private String row;
	
	public Row(String row) {
		this.row = row;
		analyseRow(row);
	}
	
	private void analyseRow(String row)
	{
		String[] RowParts = row.split("\\$");
		
		for( int i = 0; i <RowParts.length; i++)
		{
			switch(i)
			{
			case 0:
				ip =RowParts[i];
		        key = (long)Long.parseLong((ip.replace(".","")));
				break;
			case 1:
				data = RowParts[i];
				break;
			}
		}
	}
	
	public String getData()
	{
		return data;
	}
	
	public String getIP()
	{
		return ip;
	}
	
	public String getRow()
	{
		return row;
	}
	
	public long getKey()
	{
		return key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + (int) (key ^ (key >>> 32));
		result = prime * result + ((row == null) ? 0 : row.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Row other = (Row) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (key != other.key)
			return false;
		if (row == null) {
			if (other.row != null)
				return false;
		} else if (!row.equals(other.row))
			return false;
		return true;
	}
	
	

}
