package hashing;

public class TestParsing {
	
	public static void main(String[] args) {
		
		Row row = new Row("176.140.168.63$ - - [Mon Dec 07 20:15:02 CET 2015:0]");
		
		System.out.println("Entire Row: " + row.getRow());
		System.out.println("IP: " + row.getIP());
		System.out.println("Key: " + row.getKey());
		System.out.println("Data: " + row.getData());

	}

}
